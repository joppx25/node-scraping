module.exports = {
    getCredentials: function(btn) {
        let userInfo = {};
        switch(btn) {
            case 'update':
                userInfo = {
                    username: 'sample@example.com',
                    password: 'wmxdh8jf',
                    url: 'https://exams.bbo.com.ph/login',
                    crawlURL: 'https://exams.bbo.com.ph/crawler',
                    btnSelector: 'form.form-horizontal > div.form-group .col-md-8.col-md-offset-4 > button.btn.btn-primary'
                };
                break;
            case 'lazada':
                userInfo = {
                    username: 'testtestmeme101@gmail.com',
                    password: 'testtesttesttest1',
                    url: 'https://www.lazada.com.ph/customer/account/login/',
                    crawlURL: '',
                    btnSelector: 'form#form-account-login div.col2 > button.ui-button.ui-buttonCta'
                };
                break;
            case 'gitlab':
                userInfo = {
                    username: 'testing101',
                    password: 'testtesttesttest',
                    url: 'https://gitlab.com/users/sign_in/',
                    crawlURL: '',
                    btnSelector: 'form#new_user div.submit-container.move-submit-down > input.btn.btn-save'
                };
                break;
            case 'amazon':
                userInfo = {
                    username: 'testtestmeme101@gmail.com',
                    password: 'testtesttesttest1',
                    url: 'https://www.amazon.com/ap/signin?_encoding=UTF8&ignoreAuthState=1&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3F_encoding%3DUTF8%26ref_%3Dnav_ya_signin&switch_account=',
                    crawlURL: '',
                    btnSelector: '#signInSubmit'
                };
                break;
        }
        return userInfo;
    }
};