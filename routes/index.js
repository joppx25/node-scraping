const express = require('express');
const router = express.Router();
const puppeteer = require('puppeteer');
const cheerio = require('cheerio');

let username = '';
let password = '';
let url = ''; 
let crawURL = '';
let emailSelector = '';
let passwordSelector = '';

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('GET REQUEST');
  res.render('index', { data: 'Express' });
});

router.post('/', function(req, res, next) {
  let btn = Object.keys(req.body)[0];
  init().then(content => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.render('index', {data: content});
  });
});

// setUp credentials here
async function init(btn) {
  let user = require('./credentials.js');
  // let { username, password, url, crawlURL, btnSelector } = user.getCredentials(btn);
  const browser = await puppeteer.launch({
    headless: true,
  });

  const page = await browser.newPage();
  await page.setExtraHTTPHeaders({
    'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8'
  });
  
  // authenticate(username, password, url, btnSelector);
  let html = await authenticate(page, browser).then(content => {
    return content;
  });
  return await html;
}

// async function authenticate(username, password, url, selector) {
async function authenticate(page, browser) {

  await page.goto('https://www.amazon.com/ap/signin?_encoding=UTF8&ignoreAuthState=1&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3F_encoding%3DUTF8%26ref_%3Dnav_ya_signin&switch_account=');
  let loginPage = await page.evaluate(() => {
    return document.documentElement.outerHTML;
  });

  let $ = cheerio.load(loginPage);
  emailSelector = '#' + $('form').find($('input[type="email"]')).attr('id') || '#' + $('form').find($('input[type="text"]')).attr('id');
  passwordSelector = '#' + $('form').find($('input[type="password"]')).attr('id');

  await page.click(emailSelector);
  await page.keyboard.type('etransact.online@gmail.com');
  await page.click(passwordSelector);
  await page.keyboard.type('4Tfiers');
  await page.click('#signInSubmit');
  await page.waitForNavigation();

  let bodyHTML = await page.evaluate(() => document.documentElement.outerHTML);
  return await bodyHTML;
  // await browser.close();
}

module.exports = router;
